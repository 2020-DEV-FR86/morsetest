//
//  Extensions.swift
//  PolyTest
//
//  Created by Malek Mansour on 19/02/2021.
//

import Foundation

extension String {
    func encodeToMorse() -> String {
        var result = ""
        let wordArray = self.components(separatedBy: " ")
        wordArray.forEach { (word) in

            for char in word {

                if let morse = MorseDecoderData.alphaNumToMorse[String(char.uppercased())] {

                    result += morse
                }

            }
            result += "/"
        }
        result.removeLast()
        return result
    }
    func decodeMorse() -> String {
        var engOutputStr = String()
        let wordArray = self.components(separatedBy: "/")
        wordArray.forEach { (word) in
            let characterArray = word.components(separatedBy: " ")
            for character in characterArray {
                if let char = MorseDecoderData.morseToAlphaNum[character] {
                    engOutputStr += char
                } else if word != "" { // add the unknown characters '?'
                    engOutputStr += "?"
                }
            }
            engOutputStr += " "
        }
        engOutputStr.removeLast()
        return engOutputStr
    }
    func readFromStringList(from fileName: String) -> [String]? {
        var arrayOfStrings: [String]?
        do {
            // This solution assumes  you've got the file in your bundle
            if let path = Bundle.main.path(forResource: fileName, ofType: "txt") {
                let data = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                arrayOfStrings = data.components(separatedBy: "\n")
            }
        } catch let err as NSError {
            print(err)
        }
        return arrayOfStrings
    }
}
