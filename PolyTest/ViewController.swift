//
//  ViewController.swift
//  PolyTest
//
//  Created by Malek Mansour on 19/02/2021.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let list = String().readFromStringList(from: "google1000")
        list?.forEach({ (word) in
            print(word.encodeToMorse())
        })
    }
}
